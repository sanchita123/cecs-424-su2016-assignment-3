%% @author Sanchita Srivastava
%% @doc @todo Add description to partTwo.

-module(partTwo).

%% ====================================================================
%% API functions
%% ====================================================================
-export([wordlist/0,process_each_line/1,aggregate_list_tuple/1,callMap/1]).

%% ====================================================================
%% Internal functions
%% ====================================================================

% Count the number from Filename

wordlist() ->
case file:open("assign3-part2.txt", read) of
{ok, IoDevice} ->
process_each_line(IoDevice);
{error, Reason} ->
io:format("~s~n", [Reason])
end.

process_each_line(IoDevice) ->
case io:get_line(IoDevice, '') of
eof -> 
file:close(IoDevice);
{error, Reason} ->
file:close(IoDevice),
throw(Reason);
Data ->
LL=string:tokens(Data,"\,.=()<> "),LP=aggregate_list_tuple(LL),io:format("each line tuple values:~p~n",[LP]),
callMap(LP),process_each_line(IoDevice)
end.


aggregate_list_tuple(List) -> aggregate_list_tuple(List, []).

aggregate_list_tuple([], Acc) -> lists:reverse(Acc);
aggregate_list_tuple([H|T], [{H, Count}|Acc]) -> aggregate_list_tuple(T, [{H, Count+1}|Acc]);
aggregate_list_tuple([H|T], Acc) -> aggregate_list_tuple(T, [{H, 1}|Acc]).



% Prints the hashtable using the list of tuples

callMap(LP) ->
HT=maps:from_list(LP),io:format("hash table:~p~n",[HT]).







