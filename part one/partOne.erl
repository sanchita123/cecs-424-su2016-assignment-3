%% @author sanchita srivastava
%% @doc @todo Add description to partOne.


-module(partOne).

%% ====================================================================
%% API functions
%% ====================================================================
-export([distribution/4,adjustment/1]).



%% ====================================================================
%% Internal functions
%% ====================================================================



% distributes even number of candies to three students A,B and C and give the half to the right one %
%distribution(A,A,A,turn)



% Check if anybody got odd number of candies, it adds +1 to make it even %
%adjustment(E) when ((E rem 2) =/= 0) -> (E+1).
	%io:format("E is adjusted to ~p.",[E]).
adjustment(E) ->
  if ((E rem 2) =/= 0) -> (E+1);
	 true -> (E+0)
  end.


% Execution part


distribution(A1,B1,C1,Ct) ->
	
	if 
		(A1-B1 == 0) and (B1-C1 == 0) and (C1-A1 == 0) ->
			io:format("Student A has ~p.\r",[A1]),
			io:format("Student B has ~p.\r",[B1]),
			io:format("Student C has ~p.\r",[C1]),
			io:format("Total #turns : ~p.\r",[Ct]),
			io:format("All three students have same amount of candies.");
		true ->
			TV1 = adjustment(trunc((A1 + B1 - 0.5*A1))),
			TV2 = adjustment(trunc((B1 + C1 - 0.5*B1))),
			TV3 = adjustment(trunc((C1 + A1 - 0.5*C1))),
			TV4 = Ct+1, 
			distribution(TV1,TV2,TV3,TV4)
	end.